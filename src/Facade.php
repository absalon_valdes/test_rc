<?php

namespace App;

class Facade
{
    public function go($name)
    {
        $repository = new ProductRepository();

        $p1 = new Provider();
        $p1->setProducts([
            'a' => 324234,
            'b' => 3423,
            'j' => 'producto j en a'
        ]);

        $p2 = new Provider();
        $p2->setProducts([
            'd' => 324234,
            'e' => 3423,
            'f' => 4555
        ]);

        $p3 = new Provider();
        $p3->setProducts([
            'g' => 324234,
            'h' => 3423,
            'i' => 4555
        ]);

        $p4 = new Provider();
        $p4->setProducts([
            'j' => 'producto j',
            'k' => 'producto k',
            'l' => 'producto l slasdd'
        ]);

        $repository->addProvider($p1);
        $repository->addProvider($p2);
        $repository->addProvider($p3);
        $repository->addProvider($p1);

        return $repository->get($name);

    }
}