<?php

namespace App;

/**
 * Class ProductRepository
 * @package App
 */
class ProductRepository
{
    /** @var Provider[] */
    private $providers = [];

    /**
     * @param $name
     * @return mixed
     * @throws \Exception
     */
    public function get($name)
    {
        if (empty($this->providers)) {
            throw new \Exception('No hay providers registrados');
        }

        return $this->providers[0]->get($name);
    }

    /**
     * @param Provider $provider
     * @throws \Exception
     */
    public function addProvider(Provider $provider)
    {
        if (in_array($provider, $this->providers, true)) {
            throw new \Exception('Dependencia circular detectada');
        }

        /** @var Provider $last */
        $last = end($this->providers);

        $this->providers[] = $provider;

        if ($last !== $provider && $last instanceof Provider) {
            $last->setNext($provider);
        }
    }

    /**
     * @return Provider[]
     */
    public function getProviders()
    {
        return $this->providers;
    }
}