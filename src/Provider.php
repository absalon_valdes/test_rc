<?php

namespace App;

/**
 * Class Provider
 * @package App
 */
class Provider
{
    /** @var Provider */
    private $next;
    /**
     * @var
     */
    private $products;

    /**
     * @param $name
     * @return null
     */
    public function get($name)
    {
        if (isset($this->products[$name])) {
            return $this->products[$name];
        }

        if ($this->next === null) {
            return null;
        }

        return $this->next->get($name);
    }

    /**
     * @param Provider $next
     */
    public function setNext(Provider $next)
    {
        $this->next = $next;
    }

    /**
     * @return Provider
     */
    public function getNext()
    {
        return $this->next;
    }

    /**
     * @param array $products
     */
    public function setProducts(array $products)
    {
        $this->products = $products;
    }
}