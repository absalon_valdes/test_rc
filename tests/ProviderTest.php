<?php

namespace App\Tests\Unit;

use App\Provider;
use PHPUnit\Framework\TestCase;

class ProviderTest extends TestCase
{
    /** @var Provider */
    private $provider;

    protected function setUp()
    {
        $this->provider = new Provider();
        $this->provider->setProducts([
            'producto' => new \stdClass,
        ]);
    }

    public function test_devuelve_null_si_no_tiene_item_o_no_tiene_next()
    {
        self::assertNull($this->provider->getNext());
        self::assertNull($this->provider->get('producto_no_existen'));
    }

    public function test_devuelve_producto_si_lo_tiene()
    {
        self::assertNotNull($this->provider->get('producto'));
    }

    public function test_llamar_next_si_no_existe_producto()
    {
        $next = $this->prophesize(Provider::class);
        $next->get('producto_no_existe')->shouldBeCalled();

        $this->provider->setNext($next->reveal());
        self::assertInstanceOf(Provider::class, $this->provider->getNext());

        $this->provider->get('producto_no_existe');
    }
}

