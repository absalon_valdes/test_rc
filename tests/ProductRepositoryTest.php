<?php

namespace App\Tests\Unit;

use App\ProductRepository;
use App\Provider;
use PHPUnit\Framework\TestCase;

class ProductRepositoryTest extends TestCase
{
    /**
     * @expectedException \Exception
     * @expectedExceptionMessage No hay providers registrados
     */
    public function test_excepcion_si_no_hay_providers()
    {
        $repositorio = new ProductRepository();
        $repositorio->get('televisor');
    }

    public function test_agregar_providers()
    {
        $repositorio = new ProductRepository();

        $repositorio->addProvider(new Provider());
        $repositorio->addProvider(new Provider());
        $repositorio->addProvider(new Provider());

        self::assertCount(3, $repositorio->getProviders());
    }

    public function test_cadena_responsabilidad()
    {
        $provider1 = $this->prophesize(Provider::class);
        $provider2 = $this->prophesize(Provider::class);
        $provider3 = $this->prophesize(Provider::class);

        $provider1->setNext($provider2->reveal())->shouldBeCalled();
        $provider2->setNext($provider3->reveal())->shouldBeCalled();

        $repositorio = new ProductRepository();
        $repositorio->addProvider($provider1->reveal());
        $repositorio->addProvider($provider2->reveal());
        $repositorio->addProvider($provider3->reveal());
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Dependencia circular detectada
     */
    public function test_dependencia_circular()
    {
        $provider1 = $this->prophesize(Provider::class);
        $provider2 = $this->prophesize(Provider::class);
        $provider3 = $this->prophesize(Provider::class);

        $repositorio = new ProductRepository();
        $repositorio->addProvider($provider1->reveal());
        $repositorio->addProvider($provider2->reveal());
        $repositorio->addProvider($provider3->reveal());
        $repositorio->addProvider($provider1->reveal());
    }

    public function test_llama_provider_si_existe()
    {
        $dummyProvider = $this->prophesize(Provider::class);
        $dummyProvider->get('televisor')->shouldBeCalled();

        $repositorio = new ProductRepository();
        $repositorio->addProvider($dummyProvider->reveal());
        $repositorio->get('televisor');
    }
}
